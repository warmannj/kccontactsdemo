//
//  KCContactsTests.m
//  KCContactsTests
//
//  Created by John Warmann on 2014-05-29.
//  Copyright (c) 2014 Cell Innovation. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "KCContactsManager.h"
#import "GDataEntryContact+DisplayName.h"

@interface KCContactsTests : XCTestCase
{
    KCContactsManager *contactsManager;
    NSString *testUser;
    NSString *testPass;
}
@end

@implementation KCContactsTests

- (void)setUp
{
    [super setUp];
    contactsManager = [KCContactsManager sharedManager];
    testUser = @"kctestuser@gmail.com";
    testPass = @"kctestpass";
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testSingleton
{
    XCTAssertNotNil(contactsManager, @"Defaul ContactsManager is nil");
    XCTAssertEqual(contactsManager, [KCContactsManager sharedManager], @"Two shared contactsManagers not equal");
}

// This test would require creating test user account and signing in with that.
// With more time would set up the test to pass.
- (void) testSortedEntries
{
    [contactsManager loginWithEmail:testUser password:testPass completion:^{
        NSArray *sortedEntries =  [contactsManager sortedEntries:contactsManager.contacts];
        XCTAssertEqual(sortedEntries.count, contactsManager.contacts.count, @"Count not equal");
        GDataEntryContact *previousEntry = nil;
        for (GDataEntryContact *entry in sortedEntries) {
            if ([[entry entryDisplayName] caseInsensitiveCompare:[previousEntry entryDisplayName]] == NSOrderedDescending) {
                XCTFail(@"SortedEntries not correctly sorted:%@ > %@", entry, previousEntry);
            }
            previousEntry = entry;
        }
    }];
}

@end
