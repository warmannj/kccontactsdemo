//
//  KCMasterViewController.h
//  KCContacts
//
//  Created by John Warmann on 2014-05-29.
//  Copyright (c) 2014 Cell Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KCDetailViewController;

@interface KCMasterViewController : UITableViewController

@property (strong, nonatomic) KCDetailViewController *detailViewController;

@end
