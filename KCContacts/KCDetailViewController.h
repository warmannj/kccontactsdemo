//
//  KCDetailViewController.h
//  KCContacts
//
//  Created by John Warmann on 2014-05-29.
//  Copyright (c) 2014 Cell Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GDataEntryContact;

@interface KCDetailViewController : UIViewController <UISplitViewControllerDelegate>

@property (strong, nonatomic) id detailItem;

@property (strong, nonatomic) GDataEntryContact *contact;
@property (weak, nonatomic) IBOutlet UILabel *fullName;
@property (weak, nonatomic) IBOutlet UILabel *emails;
@property (weak, nonatomic) IBOutlet UILabel *phones;

@end
