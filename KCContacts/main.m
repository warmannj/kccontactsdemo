//
//  main.m
//  KCContacts
//
//  Created by John Warmann on 2014-05-29.
//  Copyright (c) 2014 Cell Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KCAppDelegate class]));
    }
}
