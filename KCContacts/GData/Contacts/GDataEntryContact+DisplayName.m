//
//  GDataEntryContact+DisplayName.m
//  KCContacts
//
//  Created by John Warmann on 2014-05-30.
//  Copyright (c) 2014 Cell Innovation. All rights reserved.
//

#import "GDataEntryContact+DisplayName.h"

static const unichar kBallotX = 0x2717; // fancy X mark to indicate deleted items

@implementation GDataEntryContact (DisplayName)


- (NSString *)entryDisplayName {
    
    NSString *title;
    
    if ([self isDeleted]) {
        title = [NSString stringWithFormat:@"%C %@", kBallotX, [self identifier]];
    } else {
        
        title = [[self title] stringValue];
        
        // if no title, fall back on e-mail address or use the string "Contact N"
        if ([title length] == 0 && [[self emailAddresses] count] > 0) {
            
            GDataEmail *email = [self primaryEmailAddress];
            if (email == nil) {
                email = [[self emailAddresses] objectAtIndex:0];
            }
            title = [email address];
        }
        
        if ([title length] == 0) {
            // fallback case
            title = [self description]; 
        }
    }
    
    return title;
}

@end
