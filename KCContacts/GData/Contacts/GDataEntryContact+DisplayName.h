//
//  GDataEntryContact+DisplayName.h
//  KCContacts
//
//  Created by John Warmann on 2014-05-30.
//  Copyright (c) 2014 Cell Innovation. All rights reserved.
//

#import "GDataEntryContact.h"

@interface GDataEntryContact (DisplayName)
- (NSString *)entryDisplayName;
@end
