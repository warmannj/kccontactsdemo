//
//  KCLoginViewController.m
//  KCContacts
//
//  Created by John Warmann on 2014-05-30.
//  Copyright (c) 2014 Cell Innovation. All rights reserved.
//

#import "KCLoginViewController.h"
#import "KCContactsManager.h"

@interface KCLoginViewController ()

@end

@implementation KCLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)doLogin:(id)sender
{
    NSString *email = self.emailField.text;
    NSString *password = self.passwordField.text;
    
    if (!email || !email.length) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invalid Email"
                                                        message:@"Enter a valid email address"
                                                       delegate:nil cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
        [alert  show];
    }
    
    if (!password || !password.length) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Please enter your password."
                                                       delegate:nil cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert  show];
    }
    
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    
    email = [email stringByTrimmingCharactersInSet:whitespace];
    
    if ([email rangeOfString:@"@"].location == NSNotFound) {
        // if no domain was supplied, add @gmail.com
        email = [email stringByAppendingString:@"@gmail.com"];
    }
    
    self.emailField.text = email;
    
    [[KCContactsManager sharedManager] loginWithEmail:email password:password completion:self.contactsFetchedCompletionHandler];
    
    [self dismiss:sender];
}

- (IBAction)dismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.emailField) {
        [textField resignFirstResponder];
        [self.passwordField becomeFirstResponder];
    } else if (textField == self.passwordField) {
        [textField resignFirstResponder];
        [self doLogin:textField];
    }
    return YES;
}

@end
