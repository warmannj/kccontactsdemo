//
//  KCLoginViewController.h
//  KCContacts
//
//  Created by John Warmann on 2014-05-30.
//  Copyright (c) 2014 Cell Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KCMasterViewController.h"
#import "KCContactsFetchedCompletion.h"

@interface KCLoginViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;

@property (assign) ContactFetchedBlock contactsFetchedCompletionHandler;

- (IBAction)doLogin:(id)sender;
- (IBAction)dismiss:(id)sender;

@end
