//
//  KCContactsManager.m
//  KCContacts
//
//  Created by John Warmann on 2014-05-30.
//  Copyright (c) 2014 Cell Innovation. All rights reserved.
//

#import "KCContactsManager.h"
#import "GDataContacts.h"

@interface KCContactsManager ()
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) GDataFeedContact *contactFeed;

- (void)fetchAllContactsWithCompletion:(ContactFetchedBlock)completionHandler;
- (NSArray *)sortedEntries:(NSArray *)entries;
@end

@implementation KCContactsManager

@synthesize contacts;

#pragma mark - Initialization

// Return singleton instance of this class
+ (id)sharedManager
{
    static KCContactsManager *sharedMyManager = nil;
    
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init
{
    self = [super init];
    
    if (self) {
        contacts = [[NSMutableArray alloc] init];
    }
    return self;
}

#pragma mark - Loging in

- (void) loginWithEmail:(NSString*)username password:(NSString*)password completion:(ContactFetchedBlock)completionHandler
{
    self.username = username;
    self.password = password;
    
    [self fetchAllContactsWithCompletion:completionHandler];
}

- (void) deleteContact:(GDataEntryContact*)entry  withCompletion:(ContactFetchedBlock)completionHandler
{
    GDataServiceGoogleContact *service = [self contactService];
        
    [service deleteEntry:entry completionHandler:^(GDataServiceTicket *ticket, id nilObject, NSError *error) {
        completionHandler();
    }];
}

#pragma mark - Helpers

- (GDataServiceGoogleContact *)contactService
{
    static GDataServiceGoogleContact* service = nil;
    
    if (!service) {
        service = [[GDataServiceGoogleContact alloc] init];
        
        [service setShouldCacheResponseData:YES];
        [service setServiceShouldFollowNextLinks:YES];
    }
    
    [service setUserCredentialsWithUsername:self.username
                                   password:self.password];
    
    return service;
}

- (void)fetchAllContactsWithCompletion:(ContactFetchedBlock)completionHandler
{
    GDataServiceGoogleContact *service = [self contactService];
    GDataServiceTicket *ticket;
    
    // request a whole buncha contacts; our service object is set to
    // follow next links as well in case there are more than 2000
    const int kBuncha = 500;
    
    NSURL *feedURL = [self contactFeedURL];
    
    GDataQueryContact *query = [GDataQueryContact contactQueryWithFeedURL:feedURL];
    [query setShouldShowDeleted:NO];
    [query setMaxResults:kBuncha];
    
    ticket = [service fetchFeedWithQuery:query completionHandler:^(GDataServiceTicket *ticket, GDataFeedBase *feed, NSError *error) {
        NSLog(@"FeedBase = %@", feed);
        [self.contacts removeAllObjects];
        if (error) {
            // This alert would be moved to view controller in more time and passed through with completion handler callback
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error retrieving contacts"
                                                            message:error.localizedDescription
                                                           delegate:nil cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            //completion handler would be called with nil passed in
        } else if (feed.entries && feed.entries.count) { // No error and we have some contacts
            self.contacts = [[NSMutableArray alloc] initWithArray: [self sortedEntries:feed.entries]];
            completionHandler();
        }
    }];
}


- (NSURL *)groupFeedURL
{
    NSURL *feedURL = [GDataServiceGoogleContact groupFeedURLForUserID:kGDataServiceDefaultUser];
    return feedURL;
}

- (NSURL *)contactFeedURL
{
    NSURL *feedURL = [GDataServiceGoogleContact contactFeedURLForUserID:kGDataServiceDefaultUser];
    return feedURL;
}

- (NSArray *)sortedEntries:(NSArray *)entries
{
    
    SEL sel = @selector(caseInsensitiveCompare:);
    
    NSSortDescriptor *sortDesc = [[NSSortDescriptor alloc] initWithKey:@"entryDisplayName"
                                                             ascending:YES
                                                              selector:sel];
    
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDesc];
    entries = [entries sortedArrayUsingDescriptors:sortDescriptors];
    
    return entries;
}

@end
