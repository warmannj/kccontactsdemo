//
//  KCContactsManager.h
//  KCContacts
//
//  Created by John Warmann on 2014-05-30.
//  Copyright (c) 2014 Cell Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KCContactsFetchedCompletion.h"

@class GDataEntryContact;

@interface KCContactsManager : NSObject

@property (nonatomic, strong) NSMutableArray *contacts;

+ (id) sharedManager;
- (void) loginWithEmail:(NSString*)email password:(NSString*)password completion:(ContactFetchedBlock)completionHandler;
- (void) deleteContact:(GDataEntryContact*)entry  withCompletion:(ContactFetchedBlock)completionHandler;
- (NSArray *)sortedEntries:(NSArray *)entries;
@end
