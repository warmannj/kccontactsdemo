//
//  ContactsFetchedCompletion.h
//  KCContacts
//
//  Created by John Warmann on 2014-05-30.
//  Copyright (c) 2014 Cell Innovation. All rights reserved.
//

#ifndef KCContacts_ContactsFetchedCompletion_h
#define KCContacts_ContactsFetchedCompletion_h

typedef void (^ContactFetchedBlock)(void);

#endif
