//
//  KCDetailViewController.m
//  KCContacts
//
//  Created by John Warmann on 2014-05-29.
//  Copyright (c) 2014 Cell Innovation. All rights reserved.
//

#import "KCDetailViewController.h"
#import "GDataEntryContact+DisplayName.h"

@interface KCDetailViewController ()
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
- (void)configureView;
@end

@implementation KCDetailViewController

#pragma mark - Managing the detail item

- (void)setContact:(GDataEntryContact *)newContact
{
    if (_contact != newContact) {
        _contact = newContact;
        
        [self configureView];
    }

    if (self.masterPopoverController != nil) {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }        
}

- (void)configureView
{
    // Update the user interface for the detail item.

    if (self.contact) {
        self.fullName.text = self.contact.name.fullName.stringValue;
        self.emails.text  = self.contact.primaryEmailAddress.address;
        self.phones.text = self.contact.primaryPhoneNumber.stringValue;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
