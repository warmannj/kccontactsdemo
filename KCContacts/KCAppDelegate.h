//
//  KCAppDelegate.h
//  KCContacts
//
//  Created by John Warmann on 2014-05-29.
//  Copyright (c) 2014 Cell Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
