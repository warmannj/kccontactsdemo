//
//  KCContactTableViewCell.h
//  KCContacts
//
//  Created by John Warmann on 2014-05-30.
//  Copyright (c) 2014 Cell Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KCContactTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *nameField;
@property (nonatomic, weak) IBOutlet UILabel *numberField;
@property (nonatomic, weak) IBOutlet UILabel *emailField;
@end
